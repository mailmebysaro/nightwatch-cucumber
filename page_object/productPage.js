var data = require('../data/test_datas.json');
module.exports = {

  url: function() { 
    return this.api.globals.site_url + '/'; 
  },

  elements: {
      body: 'body',
      giftFinderForm: '.gift-finder-form',      
      zipcode:"#zipcode-giftfinder",      
      datePicker: '.daypicker',
      datePickerInput: 'div.datepicker input',
      selectDate: 'div.DayPicker-Day--today ~ div>button:not(.unavailableDate)',
      submitBtn: '.btn-submit',
      searchResultsBar: '.product-bar',
      selectProduct: {
        selector: "a[href*='/product/']",
        index: 2
      },
      delivaryOption: "div.delivery-options",
      delivaryCloseBtn: '.close-btn',
      closeIcon: '#button3',
      addToCart: 'button[type=submit]',
      addedToCartConfirmBox: '.added-to-cart-summary .added-to-cart-summary',
      checkout: '.btn-wide',
      firstName: '#first-name-1',
      lastName: '#last-name-1',
      address: '#address1-1',
      city: '#city-1',
      phoneNumber: '#phone-1',
      continueToPayment: 'button.btn-primary[type=button]'
  }

}