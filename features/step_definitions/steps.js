const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');
const assert = require('assertthat');
var data = require('../../data/test_datas.json');

defineSupportCode(({Given, Then, When}) => {

  defineSupportCode(({After, Before}) => {

    Before(() => {

      return client.init().maximizeWindow();

    });

    After(() => {

      return client.end();

    });

  });
  
  Given('I open home page', () => {

    var productPage = client.page.productPage();
    productPage.navigate()
    .assert.title(data.title)
    .assert.visible('@giftFinderForm')
    .assert.visible('@zipcode')
    .assert.visible('@submitBtn')   

  });
 
  //When Steps

  When('I search items', () => {

    var productPage =  client.page.productPage()
    productPage.setValue('@zipcode', data.zip);
    client.pause(40000);
    client.element("css selector", "iframe[id*='lightbox-iframe']", (response) => {  
      if(response.status != -1){    
      client.frame({ELEMENT: response.value.ELEMENT}, (found)=> {       
        client.click('#button3')
              .frameParent()
      })   
    }       
    });    
    client.pause(3000);
    productPage.click('@datePickerInput');
    client.pause(2000);
    client.element("css selector", "iframe[id*='lightbox-iframe']", (response) => {    
      if(response.status != -1)  {
      client.frame({ELEMENT: response.value.ELEMENT}, (found)=> {        
        client.click('#button3')
              .frameParent()
      })          
    }
    });
    productPage.waitForElementVisible('@datePicker', 5000)
    .click('@selectDate')
    client.pause(3000);
    client.element("css selector", "iframe[id*='lightbox-iframe']", (response) => {    
      if(response.status != -1)  {
      client.frame({ELEMENT: response.value.ELEMENT}, (found)=> {        
        client.click('#button3')
              .frameParent()
      }) 
    }         
    });
    productPage.click('@submitBtn')
    client.pause(10000);

  });

  Then('I should see list of available items', () => {

    var productPage =  client.page.productPage()
    productPage.waitForElementVisible('@selectProduct', 10000, false, function(result){
      productPage.click('@selectProduct');
    });
      client.element("css selector", "iframe[id*='lightbox-iframe']", (response) => {    
        if(response.status != -1) {  
        client.frame({ELEMENT: response.value.ELEMENT}, (found)=> {        
          client.click('#button3')
                .frameParent()
        })    
      }      
      });
     
  });

  Then('I select an item and add to cart', () => {

    client.pause(10000);
    client.element("css selector", "iframe[id*='lightbox-iframe']", (response) => {     
      if(response.status != -1) { 
      client.frame({ELEMENT: response.value.ELEMENT}, (found)=> {        
        client.click('#button3')
              .frameParent()
      }) 
    }         
    });
    var productPage =  client.page.productPage()
    client.element("css selector", ".close-btn", (response) => { 
      console.log('@@@@@@ response' +response.status)
      if(response.status != -1)
        client.click('.close-btn')
    });
    productPage.waitForElementVisible('@addToCart', 10000, false, function(result){
      console.log('@@@@@@ cart btn cisible')
     
    productPage.click('@addToCart');
    });

  });

  Then(/^I checkout the item$/, () => {

    client.pause(10000);
    var productPage =  client.page.productPage()
    productPage.waitForElementVisible('@checkout', 10000, false, function(result){
      productPage.click('@checkout');
      productPage.waitForElementVisible('@firstName', 15000, false, function(result){
        productPage.setValue('@firstName', data.firstName)
                  .setValue('@lastName', data.lastName)
                  .setValue('@address', data.address)
                  .setValue('@city', data.city)
                  .setValue('@phoneNumber', data.phone)
                  .click('@continueToPayment')  
      });
    });

  });
 
});
