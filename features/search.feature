@search
Feature: search items by date
  In order to be able search items by date on the given zip
  As an user 
  I need to be able to analyze results

  Scenario: User can search items by date
    Given I open home page
    When I search items
    Then I should see list of available items
    And I select an item and add to cart
    And I checkout the item